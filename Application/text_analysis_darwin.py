import pandas as pd
from nltk.tokenize import sent_tokenize
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from amb_sdk.sdk import DarwinSdk
from time import sleep

class Text():
    def __init__(self, text):
        self.text = text
        self.sentences = sent_tokenize(self.text)

        # Vectorized data, ready for Darwin
        self.poli_data = self.prep_text("political_bias_model_V2")
        self.bot_data = self.prep_text("bot_detection_model_v3")

        # Scores
        self.sent = self.determine_sent()
        self.poli_bias = self.determine_bias("political_bias_model_V2")
        self.bot_prob = self.determine_bot("bot_detection_model_v3")
        self.score = self.score()

    # Clean and vectorize text
    # Vectorization will be different, needs the same attributes as the training set
    # return a pandas dataframe
    def prep_text(self, mode):
        print("Prepping text (" + mode + ")...")

        # Make text into DataFrame
        df = pd.DataFrame(self.sentences, columns = ['text'])

        # Remove non-ASCII characters
        df['text'] = df['text'].apply(lambda x: ''.join([" " if ord(i) < 32 or ord(i) > 126 else i for i in x]))

        # Perform count vectorization
        count_vect = CountVectorizer()
        text_counts = count_vect.fit_transform(df['text'])

        # Transform to TF-IDF
        tfidf = TfidfTransformer()
        text_tfidf = tfidf.fit_transform(text_counts)
        text_tfidf.shape

        # Concat DataFrames
        tfidf_df = pd.DataFrame(text_tfidf.todense(), columns=count_vect.get_feature_names())
        combined_df = pd.concat([df, tfidf_df], axis=1)
        df = combined_df

        # Read in features from training set
        filename = './features/' + mode + '_features.txt'
        with open(filename, 'r') as f:
            features = [line.rstrip('\n') for line in f]

        # Add in all other features from training set (necessary for Darwin to work)
        for feat in features:
            if feat not in df.columns:
                df[feat] = 0.0

        # Drop features not in training set (again, for Darwin)
        for c in df.columns:
            if c not in features:
                df.drop([c], axis=1)

        df.to_csv(mode.split("_")[0] + "_data.csv", index=False)
        return df

    # Sentiment analysis of text
    # Determines overall magnitude of any sentiment, irrespective of class
    def determine_sent(self):
        print("Performing sentiment analysis...")
        sia = SentimentIntensityAnalyzer()
        pos, neg = 0,0
        for s in self.sentences:
            pos += sia.polarity_scores(s)['pos']
            neg += sia.polarity_scores(s)['neg']
        return round(((pos + neg) / 2), 4)

    def determine_bias(self, model):
        print("Determining political bias...")

        # Set login info
        USER = 'kevin.han@utexas.edu'
        PASS = 'ZN99wf8AuY'
        # Login
        ds = DarwinSdk()
        ds.set_url('https://amb-demo-api.sparkcognition.com/v1/')
        status, msg = ds.auth_login_user(USER, PASS)
        ds.delete_all_datasets()
        #ds.delete_all_artifacts()

        # Upload dataset
        test_data = "political_data.csv"
        ds.delete_dataset(test_data)
        status, dataset = ds.upload_dataset(test_data)
        target = "leaning"

        # Clean test dataset
        sleep(6)
        print("Cleaning dataset...")
        status, job_id = ds.clean_data(test_data, target = target, model_name = model)
        if status:
            ds.wait_for_job(job_id['job_name'])
        else:
            print(job_id)

        # Run Darwin model
        sleep(10)
        print("Running model...")
        status, artifact = ds.run_model(test_data, model)
        sleep(1)
        ds.wait_for_job(artifact['job_name'])

        status, prediction = ds.download_artifact(artifact['artifact_name'])

        # average prediction scores
        right = prediction['prob_right'].mean()
        left = prediction['prob_left'].mean()
        if right > left:
            return {'leaning':'right', 'score':round(right-left, 4)}
        else:
            return {'leaning':'left', 'score':round(left-right, 4)}

    def determine_bot(self, model):
        print("Determine bot or not...")

        # Set login info
        USER = 'alexander.chan088@gmail.com'
        PASS = 'LQdQSb9X32'
        # Login
        ds = DarwinSdk()
        ds.set_url('https://amb-demo-api.sparkcognition.com/v1/')
        status, msg = ds.auth_login_user(USER, PASS)
        ds.delete_all_datasets()
        #ds.delete_all_artifacts()

        # Upload dataset
        test_data = "bot_data.csv"
        ds.delete_dataset(test_data)
        status, dataset = ds.upload_dataset(test_data)
        target = "bot-label"

        # Clean test dataset
        sleep(6)
        print("Cleaning dataset...")
        status, job_id = ds.clean_data(test_data, target = target, model_name = model)
        if status:
            ds.wait_for_job(job_id['job_name'])
        else:
            print(job_id)

        # Run Darwin model
        sleep(10)
        print("Running model...")
        status, artifact = ds.run_model(test_data, model)
        sleep(1)
        ds.wait_for_job(artifact['job_name'])

        status, prediction = ds.download_artifact(artifact['artifact_name'])

        # average prediction scores
        bot = prediction['prob_bot'].mean()
        not_bot = prediction['prob_not-bot'].mean()
        if bot > not_bot:
            return {'bot_or_not':'bot', 'score':round(bot-not_bot, 4)}
        else:
            return {'bot_or_not':'not-bot', 'score':round(not_bot-bot, 4)}

    def score(self):
        return round((self.sent + self.poli_bias['score'] + self.bot_prob['score']) / 3, 4)

    def __str__(self):
        rs = ("Text Analysis Results:\n\n"
        +"Sentiment Score: " + str(self.sent) + '\n'
        +"Political Bias: " + self.poli_bias['leaning'] + " with probability " + str(self.poli_bias['score']) + '\n'
        +"Bot or Not? " + self.bot_prob['bot_or_not'] + " with probability " + str(self.bot_prob['score']) + '\n\n'
        +"Overall Score: " + str(self.score))

        return (rs)
