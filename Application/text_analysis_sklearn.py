import pandas as pd
from nltk.tokenize import sent_tokenize
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import pickle
from statistics import mean
from newspaper import Article

class Text():
    def __init__(self, text):
        self.text = text
        self.sentences = sent_tokenize(self.text)

        # Vectorized data, ready for Darwin
        self.poli_data = self.prep_text("political")
        self.bot_data = self.prep_text("bot")

        # Scores
        self.sent = self.determine_sent()
        self.poli_bias = self.determine_bias("political_model.sav")
        self.bot_prob = self.determine_bot("bot_model.sav")
        self.score = self.score()

    # Clean and vectorize text
    # Vectorization will be different, needs the same attributes as the training set
    # return a pandas dataframe
    def prep_text(self, mode):
        print("Prepping text (" + mode + ")...")

        # Detect if article link was given instead of raw text
        if "http://" in self.text or "https://" in self.text:
            print("Article detected - downloading and parsing...")
            url = self.text
            article = Article(url)
            article.download()
            article.parse()

            self.text = article.text
            self.sentences = sent_tokenize(self.text)

        # Make text into DataFrame
        df = pd.DataFrame(self.sentences, columns = ['text'])

        # Remove non-ASCII characters
        df['text'] = df['text'].apply(lambda x: ''.join([" " if ord(i) < 32 or ord(i) > 126 else i for i in x]))

        # Perform count vectorization
        count_vect = CountVectorizer()
        text_counts = count_vect.fit_transform(df['text'])

        # Transform to TF-IDF
        tfidf = TfidfTransformer()
        text_tfidf = tfidf.fit_transform(text_counts)
        text_tfidf.shape

        # Concat DataFrames
        tfidf_df = pd.DataFrame(text_tfidf.todense(), columns=count_vect.get_feature_names())
        combined_df = pd.concat([df, tfidf_df], axis=1)
        df = combined_df

        # Read in features from training set
        filename = './features/' + mode + '_features_sklearn.txt'
        with open(filename, 'r') as f:
            features = [line.rstrip('\n') for line in f]

        for feat in features:
            if feat not in df.columns:
                df[feat] = 0.0

        # Drop features not in training set (again, for Darwin)
        for c in df.columns:
            if c not in features:
                df = df.drop([c], axis=1)

        df.sort_index(axis=1, inplace=True)
        return df


    # Sentiment analysis of text
    # Determines overall magnitude of any sentiment, irrespective of class
    def determine_sent(self):
        print("Performing sentiment analysis...")
        sia = SentimentIntensityAnalyzer()
        pos, neg = 0,0
        for s in self.sentences:
            pos += sia.polarity_scores(s)['pos']
            neg += sia.polarity_scores(s)['neg']
        pos /= len(self.sentences)
        neg /= len(self.sentences)
        return round(((pos + neg) / 2), 4)

    def determine_bias(self, model):
        print("Determining political bias...")

        # Open the model
        clf = pickle.load(open('./models/'+model, 'rb'))

        # Make predictions
        predictions = clf.predict_proba(self.poli_data)

        # Get probability for each class
        right_prob = mean([float(p.flat[0]) for p in predictions])
        left_prob = mean([float(p.flat[1]) for p in predictions])

        # Compare and choose class
        if right_prob > left_prob:
            return {'leaning':'right', 'score':round(right_prob-left_prob, 4)}
        else:
            return {'leaning':'left', 'score':round(left_prob-right_prob, 4)}

    def determine_bot(self, model):
        print("Determine bot or not...")

        # Open the model
        clf = pickle.load(open('./models/'+model, 'rb'))

        # Make predictions
        predictions = clf.predict_proba(self.bot_data)

        # Get probability for each class
        bot = mean([float(p.flat[0]) for p in predictions])
        not_bot = mean([float(p.flat[1]) for p in predictions])

        # Compare and choose class
        if bot > not_bot:
            return {'bot_or_not':'bot', 'score':round(bot-not_bot, 4)}
        else:
            return {'bot_or_not':'not-bot', 'score':round(not_bot-bot, 4)}

    def score(self):
        return round((self.sent + self.poli_bias['score'] + self.bot_prob['score']) / 3, 4)

    def __str__(self):
        rs = ("Text Analysis Results:\n\n"
        +"Sentiment Score: " + str(self.sent) + '\n'
        +"Political Bias: " + self.poli_bias['leaning'] + " with probability " + str(self.poli_bias['score']) + '\n'
        +"Bot or Not? " + self.bot_prob['bot_or_not'] + " with probability " + str(self.bot_prob['score']) + '\n\n'
        +"Overall Score: " + str(self.score))

        return (rs)
