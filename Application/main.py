from flask import Flask, render_template, flash, request
from text_analysis_sklearn import Text

# App config.
DEBUG = True
app = Flask(__name__)

@app.route("/")
def home():

    return render_template('index.html')


@app.route("/use_model", methods=["GET", "POST"])
def use_model():
    text = request.form['model_text']
    if text == '' or text == None:
        return render_template('index.html')
    else:
        results = Text(text)
        return render_template('results.html', results = results)

if __name__ == "__main__":
    app.run(debug = True)
