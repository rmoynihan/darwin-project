Shade of Yellow

Welcome to this wonderful project, done by:
Alex Chan
Megan Chan
Kevin Han
and Riley Moynihan

By training several models over data like Reddit comments, Tweets, and opinion lexicons, we created a system to detect political bias and astroturfing in text.