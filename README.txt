Shade of Yellow - Project Guide

Welcome to this wonderful project, done by:
Alex Chan
Megan Chan
Kevin Han
and Riley Moynihan

You can read the report for a comprehensive description of what we did and the vast potential
business applications.

Project Navigation Guide

•	Application/
  o	main.py
    	This is the initialization script for our webapp. Run this to use the webapp locally (note, may require you to install many Python libraries to work)
  o	text_analysis_[darwin/sklearn].py
    	Part of the webapp. These files are where the inputed text is run through our models.
  o	templates/
    	Folder where the HTML/Jinja2 templates are held
  o	models/
    	Where sklearn models have been pickled and stored
  o	features/
    	Where the lists of features that word vectorization resulted in are stored

•	DataScience/
  o	[political_data/bot_detection]_raw.csv
    	Raw, unvectorized text data with respective labels
  o	[political_data/bot_detection]_processed.csv
    	TF-IDF vectorized data, ready to be used by Darwin
  o	[political_data/bot_detection]_test.csv
    	A subset of above, not used in any training
  o	[political_leaning/bot_detection].ipynb
    	Jupyter Notebooks where data processing and Darwin training/testing occurred
  o	NotebookHTMLs/
    	HTML versions of above (better for reading only)
