import lzma
import json
import csv

INFILE = 'RC_2018-10.xz'
OUTFILE = 'reddit_test.csv'
#SUBREDDITS = {'Conservative':'right', 'Republican':'right', 'progressive':'left', 'democrats':'left', 'politics':'left'}
RSUBS = {'Conservative':'right', 'Republican':'right', 'Republicans':'right'}
LSUBS = {'progressive':'left', 'democrats':'left', 'politics':'left'}
SUBREDDITS = {**RSUBS, **LSUBS}
IGNORE = ['[removed]', '[deleted]']
MINSCORE = 15
NUMRECORDS = 3000

f = lzma.open('RC_2018-10.xz', 'r')

dat = open(OUTFILE, 'w', encoding='utf-8')
writer = csv.writer(dat)
writer.writerow(['comment', 'subreddit', 'score', 'leaning'])

r_count = 0
l_count = 0

for i in range(11000):
    f.readline()

for line in f:
    parsed = json.loads(f.readline())
    if parsed['subreddit'] in SUBREDDITS.keys() and parsed['body'] not in IGNORE and int(parsed['score']) >= MINSCORE :
        leaning = SUBREDDITS[parsed['subreddit']]

        if leaning == 'right' and r_count <= NUMRECORDS/2:
            writer.writerow([parsed['subreddit'], parsed['body'], parsed['score'], leaning])
            r_count += 1
            if r_count > NUMRECORDS/2:
                SUBREDDITS = LSUBS
            if (r_count + l_count) % 100 == 0:
                print("Records collected:", r_count + l_count)
        elif leaning == 'left' and l_count <= NUMRECORDS/2:
            writer.writerow([parsed['subreddit'], parsed['body'], parsed['score'], leaning])
            l_count += 1
            if l_count > NUMRECORDS/2:
                SUBREDDITS = RSUBS
            if (r_count + l_count) % 100 == 0:
                print("Records collected:", r_count + l_count)

        #print(parsed['body'])
        #print("Subreddit:", parsed['subreddit'], "Leaning:", SUBREDDITS[parsed['subreddit']])
        #print('**********\n')

    if r_count + l_count == NUMRECORDS + 2:
        break

dat.close()
